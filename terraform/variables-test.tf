variable "myvar" {
type = string
default = "Hei Rajesh"
}

variable "mymap"{
type = map(string)
default = {
mykey = "myvalue"
}
}

variable "myList"{
type = list
default = [1,2,3]
}
